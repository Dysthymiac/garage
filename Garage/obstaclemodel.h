#ifndef OBSTACLEMODEL_H
#define OBSTACLEMODEL_H

#include <QGraphicsItem>
#include <QPainter>



class ObstacleModel : public QGraphicsItem
{
public:
    ObstacleModel();
    ObstacleModel(QRectF r);
    ObstacleModel(const ObstacleModel& o);

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setBottomRight(QPointF point);
    QPointF bottomRight();

protected:

    QColor color;
    QRectF rect;
};

#endif // OBSTACLEMODEL_H
