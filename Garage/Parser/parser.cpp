#include "header.h"

parser::parser(std::string name_of_file) {
    std::ifstream stream(name_of_file.c_str());
	std::string curr_str;
	std::vector<std::string> param = std::vector<std::string>(7);
	while (stream) {
		getline(stream, curr_str, '\n');
		int pos = -1;
		for (int i = 0; i < 7; i++) {
			param[i] = curr_str.substr(pos+1, curr_str.find_first_of(' ', pos + 1) - pos - 1);
			pos = curr_str.find_first_of(' ', pos + 1);
		}
		rules.push_back(rule(param));
	}
	stream.close();
}
