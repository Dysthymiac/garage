#ifndef _HEADER_H
#define _HEADER_H

#include "included.h"

enum line {forward=1, reverse=-1, errorl=2};
enum sensor {small, middle, big, errors};
enum angle {to_left, no, to_right, errora};

class rule {
	int in_angle_;
	line in_line_, out_line_;
	sensor left_, center_, right_;
	angle rot_wheels_;

	void get_inangle(std::string ang);
	void get_rot_wheels(std::string wheels);
	void get_sensor(sensor & s, std::string value);
	void get_line(line & l, std::string to);
public:
	rule(std::vector<std::string> params);
	int in_angle() const;
	line in_line() const;
	line out_line() const;
	angle rot_wheels() const;
	sensor left() const;
	sensor right() const;
	sensor center() const;
};

class parser {
public:
	std::deque <rule> rules;

	parser(std::string name_of_file);
};

#endif
