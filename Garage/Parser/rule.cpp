#include "header.h"
#include <stdlib.h>

void rule::get_inangle(std::string ang) {
    in_angle_ = atoi(ang.c_str()) + 1;
}

void rule::get_rot_wheels(std::string wheels) {
	if (wheels == "->")
		rot_wheels_ = to_right;
	else if (wheels == "<-")
		rot_wheels_ = to_left;
	else if (wheels == ".")
		rot_wheels_ = no;
	else
		rot_wheels_ = errora;

}

void rule::get_sensor(sensor & s, std::string value) {
	if (value == "B")
		s = big;
	else if (value == "M")
		s = middle;
	else if (value == "S")
		s = small;
	else
		s = errors;
}

void rule::get_line(line & l, std::string to) {
	if (to == "+")
		l = forward;
	else if (to == "-")
		l = reverse;
	else
		l = errorl;
}
rule::rule(std::vector<std::string> params) {
	get_line(in_line_, params[0]);
	get_inangle(params[1]);
	get_sensor(left_, params[2]);
	get_sensor(center_, params[3]);
	get_sensor(right_, params[4]);
	get_line(out_line_, params[5]);
	get_rot_wheels(params[6]);
}

int rule::in_angle() const {
	return in_angle_;
}

line rule::in_line() const {
	return in_line_;
}

line rule::out_line() const {
	return out_line_;
}

angle rule::rot_wheels() const {
	return rot_wheels_;
}

sensor rule::left() const {
	return left_;
}

sensor rule::right() const {
	return right_;
}

sensor rule::center() const {
	return center_;
}
