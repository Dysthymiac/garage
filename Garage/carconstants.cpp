#include "carconstants.h"

int CarConstants::maxVelocity = 5;
int CarConstants::carWidth = 20;
int CarConstants::carHeight = 40;

int CarConstants::sensorsRange = 40;
qreal CarConstants::sensorsAngle = 45.01;

QString CarConstants::rulesFile = "./rules.txt";
QString CarConstants::rangesFile = "./ranges.txt";
