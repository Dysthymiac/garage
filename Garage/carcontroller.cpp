#include "carcontroller.h"

#include <QtMath>
#include <QDebug>
#include "carconstants.h"

qreal CarController::getVelocity() const
{
    return velocity;
}

qreal CarController::getTheta() const
{
    return theta;
}
void CarController::updateCar()
{
    if(velocity!=0 && theta!=45)
    {
        QPointF fl = car->getLeftFrontWheel();
        QPointF bl = car->getLeftBackWheel();
        qreal a = QLineF(fl, bl).length();
        qreal angle = qDegreesToRadians(theta-45);
        qreal r = a*qTan(angle);
        qreal w = r*velocity;
        car->setRotation(car->rotation()+w*0.05);
        if(car->collidingItems().size()>0)
            car->setRotation(car->rotation()-w*0.05);

    }
    qreal rot = qDegreesToRadians(car->rotation());
    QPointF delta(velocity*qSin(rot), -velocity*qCos(rot));
    car->setPos(car->pos()+delta);
    if(car->collidingItems().size()>0)
        car->setPos(car->pos()-delta);

}
