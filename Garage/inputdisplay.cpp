#include "inputdisplay.h"


#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QDebug>
#include <QTextItem>
#include <QFont>
#include "carconstants.h"

qreal InputDisplay::getWidth() const
{
    return width;
}

void InputDisplay::setWidth(const qreal &value)
{
    width = value;
}

qreal InputDisplay::getHeight() const
{
    return height;
}

void InputDisplay::setHeight(const qreal &value)
{
    height = value;
}

InputDisplay::InputDisplay(QGraphicsItem *i):QGraphicsObject(i), width(1), height(1)
{

}

void InputDisplay::updateParams(SensorData s)
{

    sd = s;
    if(scene())
    {

        width = scene()->views()[0]->width();
        height = scene()->views()[0]->height();
        scene()->update();

    }
}



QRectF InputDisplay::boundingRect() const
{
    return shape().boundingRect();
}

QPainterPath InputDisplay::shape() const
{
    QPainterPath p;
    p.addRect(0,0,width,height);
    return QPainterPath();
}

void InputDisplay::drawBar(QPainter *painter, qreal val, qreal r, QPointF pos, qreal w, qreal h, bool topToDown)
{
    QRectF boundsRect(pos.x(), pos.y(), w, h);
    QRectF rect;
    if(topToDown)
        rect=QRectF(pos.x(), pos.y(), w, h*r);
    else
        rect=QRectF(pos.x(), pos.y()+h*(1-r), w, h*r);


    painter->setPen(Qt::black);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(boundsRect);

    painter->setPen(Qt::black);
    painter->setBrush(Qt::red);
    painter->drawRect(rect);

    painter->setPen(Qt::white);
    painter->setBrush(Qt::white);
    QFont font=painter->font() ;
    font.setPointSize (10);
    painter->setFont(font);

    QRectF textRect;
    if(topToDown)
        textRect = QRectF(pos.x(), pos.y(), w, w);
    else
        textRect = QRectF(pos.x(), pos.y()+h-w, w, w);
    painter->setCompositionMode(QPainter::CompositionMode_Difference);
    painter->drawText(textRect, Qt::AlignCenter, QString::number(val,'f', 2));
    painter->setCompositionMode(QPainter::CompositionMode_SourceOver);
}

void InputDisplay::drawAngle(QPainter *painter, QRectF rect, qreal angle)
{

    painter->setPen(Qt::black);
    painter->setBrush(Qt::NoBrush);
    painter->drawEllipse(rect);

    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::red);

    painter->drawPie(rect, 90*16, (int)(angle*16));

    painter->setPen(Qt::white);
    painter->setBrush(Qt::white);
    QFont font=painter->font() ;
    font.setPointSize (15);
    painter->setFont(font);

    QRectF textRect(rect);
    painter->setCompositionMode(QPainter::CompositionMode_Difference);
    painter->drawText(textRect, Qt::AlignCenter, QString::number(angle,'f', 1));
    painter->setCompositionMode(QPainter::CompositionMode_SourceOver);

}

void InputDisplay::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    qreal h = height - width;
    drawBar(painter, sd.front1, sd.front1/CarConstants::sensorsRange, QPointF(0, 0), width/3, h/2);
    drawBar(painter, sd.front2, sd.front2/CarConstants::sensorsRange, QPointF(width/3, 0), width/3, h/2);
    drawBar(painter, sd.front3, sd.front3/CarConstants::sensorsRange, QPointF(2*width/3, 0), width/3, h/2);
    drawBar(painter, sd.back1, sd.back1/CarConstants::sensorsRange, QPointF(0, h/2), width/3, h/2, true);
    drawBar(painter, sd.back2, sd.back2/CarConstants::sensorsRange, QPointF(width/3, h/2), width/3, h/2, true);
    drawBar(painter, sd.back3, sd.back3/CarConstants::sensorsRange, QPointF(2*width/3, h/2), width/3, h/2, true);
    drawAngle(painter, QRectF(0, h, width, width), sd.angle);
}

