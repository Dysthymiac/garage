#ifndef SENSORDATA_H
#define SENSORDATA_H

#include <QtGlobal>

class SensorData
{
public:
    SensorData();
    qreal front1, front2, front3, back1, back2, back3;
    qreal angle;
    int direction;
};

#endif // SENSORDATA_H
