#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <string>
#include <fstream>
#include <streambuf>
#include "carconstants.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    simulation = 0;
    ui->setupUi(this);
    ui->graphicsView->resetScene();
    ui->graphicsView->setSlider(ui->verticalSlider);

    CarConstants::init();
    CarConstants::loadSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionRunSimulation_triggered()
{

    //if(simulation==0||simulation->isHidden())
    //{
        simulation = new SimulationWindow(this);
        simulation->loadScene(ui->graphicsView->getSceneState());
        simulation->show();
    //}
}

void MainWindow::on_actionReset_scene_triggered()
{
    ui->graphicsView->resetScene();
}

void MainWindow::on_actionSave_scene_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save scene"), "./scene.txt", tr("Text Files (*.txt);;All files (*)"));
    if(fileName!="")
    {
        std::ofstream ofs(fileName.toStdString().c_str());
        ofs << ui->graphicsView->saveScene();
    }
}

void MainWindow::on_actionLoad_scene_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Load scene"), "./", tr("Text Files (*.txt);;All files (*)"));
    if(fileName!="")
    {
        std::ifstream ifs(fileName.toStdString().c_str());
        std::string str((std::istreambuf_iterator<char>(ifs)),
                         std::istreambuf_iterator<char>());
        ui->graphicsView->loadScene(str);
    }
}

void MainWindow::on_actionChoose_rules_file_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Load rules"), "./", tr("Text Files (*.txt);;All files (*)"));
    if(fileName!="")
    {
        CarConstants::rulesFile = fileName;
        CarConstants::saveSettings();
    }
}

void MainWindow::on_actionChoose_ranges_file_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Load ranges"), "./", tr("Text Files (*.txt);;All files (*)"));
    if(fileName!="")
    {
        CarConstants::rangesFile = fileName;
        CarConstants::saveSettings();
    }
}
