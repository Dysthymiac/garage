#ifndef CARCONSTANTS_H
#define CARCONSTANTS_H

#include <QtGlobal>
#include <QString>
#include <QSettings>

class CarConstants
{
public:
    static const int sceneWidth = 1200;
    static const int sceneHeight = 700;
    static int maxVelocity;
    static int carWidth;
    static int carHeight;

    static int sensorsRange;
    static qreal sensorsAngle;

    static QString rulesFile;
    static QString rangesFile;

    static void init()
    {
        maxVelocity = 5;
        carWidth = 20;
        carHeight = 40;

        sensorsRange = 50;
        sensorsAngle = 45.01;

        rulesFile = "./rules.txt";
        rangesFile = "./ranges.txt";
    }

    static void saveSettings()
    {
        QSettings settings("settings.ini",QSettings::IniFormat);
        settings.setValue("rules", rulesFile);
        settings.setValue("ranges", rangesFile);
    }

    static void loadSettings()
    {
        QSettings settings("settings.ini",QSettings::IniFormat);
        rulesFile = settings.value("rules", "rules.txt").toString();
        rangesFile = settings.value("ranges", "ranges.txt").toString();

    }
};

#endif // CARCONSTANTS_H
