#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "simulationwindow.h"

#include <QMainWindow>
#include <editor.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionRunSimulation_triggered();

    void on_actionReset_scene_triggered();

    void on_actionSave_scene_triggered();

    void on_actionLoad_scene_triggered();

    void on_actionChoose_rules_file_triggered();

    void on_actionChoose_ranges_file_triggered();

private:
    Ui::MainWindow *ui;
    Editor * editor;
    SimulationWindow * simulation;
};

#endif // MAINWINDOW_H
