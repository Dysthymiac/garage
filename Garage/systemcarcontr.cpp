#include"systemcarcontr.h"
#include "carconstants.h"

void SystemCarController::updateController(SensorData sd)
{
    if(repeat > 0
            && sd.front1 > minLength
            && sd.front2 > minLength
            && sd.front3 > minLength){
        --repeat;
    }    
    else{
        repeat = 0;
        std::pair<qreal,qreal> res = def->get_next(sd);
        //qDebug() << res.first;
        //qDebug() << res.second;
        theta = res.first;
        qreal tmp = velocity;
        velocity = res.second;
        if (tmp < 0 && velocity > 0)
            repeat = 8;
    }

}

void SystemCarController::reset()
{
    CarController::reset();
}

SystemCarController::SystemCarController(CarModel* c):CarController(c){
    reset();
    repeat = 0;

    parser getrules(CarConstants::rulesFile.toStdString());

    def = new defuzzification(getrules.rules);
    ParametrReader gettrapeze(CarConstants::rangesFile.toStdString(), *def);
    gettrapeze.Read();

    minLength = gettrapeze.SmallLength();
}

SystemCarController::~SystemCarController(){
    delete def;
}


