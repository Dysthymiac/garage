#ifndef OUTPUTDISPLAY_H
#define OUTPUTDISPLAY_H

#include <QGraphicsObject>

class OutputDisplay : public QGraphicsObject
{
    qreal vel;
    qreal rot;
    qreal width;
    qreal height;

public:
    OutputDisplay(QGraphicsItem *i=0);

    void updateParams(qreal v, qreal r);
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    qreal getWidth() const;
    void setWidth(const qreal &value);
    qreal getHeight() const;
    void setHeight(const qreal &value);
};

#endif // OUTPUTDISPLAY_H
