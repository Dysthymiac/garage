#ifndef RESIZENODE_H
#define RESIZENODE_H

#include "obstaclemodel.h"

#include <QGraphicsItem>
#include<QGraphicsSceneMouseEvent>
#include <QPainter>

class ResizeNode : public QGraphicsItem
{
public:
    ResizeNode(QGraphicsItem *parent, ObstacleModel * n);
    ~ResizeNode();
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);


    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

    enum { Type = UserType + 4 };

    int type() const
    {
       return Type;
    }
protected:
    ObstacleModel* obstacle;
    bool dragging;
};

#endif // RESIZENODE_H
