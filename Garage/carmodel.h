#ifndef CARMODEL_H
#define CARMODEL_H

#include <QGraphicsItem>


class CarModel : public QGraphicsItem
{

public:
    CarModel();
    CarModel(const CarModel& c);
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QPointF getRightFrontWheel();

    QPointF getLeftFrontWheel();

    QPointF getRightBackWheel();

    QPointF getLeftBackWheel();

    enum { Type = UserType + 1 };

    int type() const
    {
       return Type;
    }
    QPointF getBackCenter();
    QPointF getFrontCenter();
    bool getShowSensors() const;
    void setShowSensors(bool value);

    QList<qreal> getFrontSensors() const;

    QList<qreal> getBackSensors() const;

    void updateSensors();
protected:
    void advance();

    QColor color;
    QPolygonF polygon_shape;
    bool showSensors;
    void initPolygon();

    QList<qreal> frontSensors;
    QList<qreal> backSensors;

private:
    qreal updateSensor(QLineF l);
    QList<QLineF> frontLines;
    QList<QLineF> backLines;


};

#endif // CARMODEL_H
