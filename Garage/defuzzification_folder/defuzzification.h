#ifndef DEFUZZIFICATION_H
#define DEFUZZIFICATION_H

#include "graph.h"
#include "sensordata.h"
#include "vector"
#include "../Parser/header.h"
#include <deque>

class defuzzification
{
    deque<rule> all_rules;

    graph left_sensor;
    graph right_sensor;
    graph center_sensor;
    graph angle_to_garage;
    graph speed;
    graph angle_of_wheels;
    bool forward;
public:
    defuzzification(deque<rule> d);

    void add_left_sensor_tr(trapeze t);
    void add_right_sensor_tr(trapeze t);
    void add_center_sensor_tr(trapeze t);
    void add_angle_to_garage_tr(trapeze t);
    void add_speed_tr(trapeze t);
    void add_angle_of_wheels_tr(trapeze t);

    pair<qreal,qreal> get_next(SensorData sd);

};

qreal minimum(qreal r1,qreal r2, qreal r3, qreal r4);

#endif // DEFUZZIFICATION_H
