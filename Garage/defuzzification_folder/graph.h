#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include "trapeze.h"
#include "mypair.h"

using namespace std;

class graph
{
public:
    vector<trapeze> tr;

    graph();

    // Добавление трапеции к графику.
    void add(trapeze t);

    // Номера и степени уверенности пересеченных трапеций.
    vector<mypair> crossed_trapeze(qreal);

    // Обрезка трапеций.
    graph cut(vector<qreal>);

    // Центр масс.
    qreal center_of_mass();
};
qreal integral_f(qreal k,qreal b,qreal x1,qreal x2);

qreal integral_f2(qreal k,qreal b,qreal x1,qreal x2);

#endif // GRAPH_H
