#include "graph.h"

graph::graph()
{

}

// Добавление трапеции к графику.
void graph::add(trapeze t)
{
    tr.push_back(t);
}

// Номера и степени уверенности пересеченных трапеций.
vector<mypair> graph::crossed_trapeze(qreal x0)
{
    vector<mypair> v;
    for (int i = 0; i < tr.size(); ++i)
        if (tr[i].x_under(x0))
            v.push_back(mypair(i,tr[i].get_y(x0)));
    return v;
}

// Образка трапеций.
graph graph::cut(vector<qreal> y)
{
    graph g;
    for (int i = 0; i < tr.size(); ++i)
        if (y[i]) g.tr.push_back(tr[i].cut(y[i]));
    return g;
}

qreal graph::center_of_mass()
{
    qreal eps = 0.001;
    bool flag = true;
    qreal S,V,k,b,X;
    S = 0;
    V = 0;
    QPointF p1,p2;
    V = 0;
    QPolygonF united_polygon;
    for(int i = 0; i < tr.size(); ++i)
        united_polygon = united_polygon.united(tr[i].vmp);
    for (int i = 0; i < united_polygon.size()-2; ++i)
    {
        p1 = united_polygon[i];
        p2 = united_polygon[i+1];
        if (qAbs(p2.x() - p1.x()) > eps)
        {
            k = (p2.y() - p1.y())/(p2.x() - p1.x());
            b = p1.y() - k*p1.x();
            S += integral_f(k,b,p1.x(),p2.x());
        }
        if (qAbs(p2.y() - p1.y()) > eps)
        {
            k = (p2.x() - p1.x())/(p2.y() - p1.y());
            b = p1.x() - k*p1.y();
            if (k<0)
            {
                if (flag) X=0;
                else X = integral_f2(0,p2.x(),0,p2.y());
                flag = true;
                V  += integral_f2(k,b,p2.y(),p1.y());
                V += X;
            }
            else if(k>0)
            {
                if (!flag) X=0;
                else X = integral_f2(0,p1.x(),0,p1.y());
                flag = false;
                V -= integral_f2(k,b,p1.y(),p2.y());
                V -= X;
            }
            else if (flag)
            {
                flag = false;
                V -= integral_f2(k,b,p1.y(),p2.y());
            }
            else
            {
                flag = true;
                V += integral_f2(k,b,p2.y(),p1.y());
            }
        }
    }
    return V/(2*S);
}

qreal integral_f(qreal k,qreal b,qreal x1,qreal x2)
{
    return (k*x2*x2/2 + b*x2) - (k*x1*x1/2 + b*x1);
}

qreal integral_f2(qreal k,qreal b,qreal x1,qreal x2)
{
    return (k*k*x2*x2*x2/3 + k*b*x2*x2 + b*b*x2) - (k*k*x1*x1*x1/3 + k*b*x1*x1 + b*b*x1);
}

