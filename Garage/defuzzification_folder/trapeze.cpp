#include "trapeze.h"
#include <vector>

trapeze::trapeze(qreal x1,qreal x2,qreal x3,qreal x4)
{
    vmp.push_back(QPointF(x1,0));
    vmp.push_back(QPointF(x2,1));
    vmp.push_back(QPointF(x3,1));
    vmp.push_back(QPointF(x4,0));
}

trapeze::trapeze(qreal x1,qreal x2,qreal x3,qreal x4,qreal ybottom,qreal ytop)
{
    vmp.push_back(QPointF(x1,ybottom));
    vmp.push_back(QPointF(x2,ytop));
    vmp.push_back(QPointF(x3,ytop));
    vmp.push_back(QPointF(x4,ybottom));
}

bool trapeze::x_under(qreal x0)
{
    return (x0>=vmp[0].x() && x0<=vmp[3].x());
}

qreal trapeze::get_y(qreal x0)
{
    qreal xa = vmp[0].x();
    qreal xb = vmp[1].x();
    if (x0>=xa && x0<=xb)
    {
        if (xb-xa)
            return ((x0-xa)/(xb-xa));
        else return 1;
    }
    xa = vmp[2].x();
    xb = vmp[3].x();
    if (x0>=xa && x0<=xb)   
    {
        if (xb-xa)
            return (xb-x0)/(xb-xa);
        else return 1;
    }
    return 1;
}

trapeze trapeze::cut(qreal y0)
{
    qreal x0,x1,x2,x3,xa,xb;

    x0 = vmp[0].x();
    x3 = vmp[3].x();

    xa = vmp[0].x();
    xb = vmp[1].x();
    x1 = (y0*(xb-xa)+xa);

    xa = vmp[2].x();
    xb = vmp[3].x();
    x2 = ((1-y0)*(xb-xa)+xa);

    return trapeze(x0,x1,x2,x3,0,y0);
}
