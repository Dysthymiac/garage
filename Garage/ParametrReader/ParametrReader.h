#pragma once

#include<string>
#include<fstream>

#include".\defuzzification_folder\trapeze.h"
#include".\defuzzification_folder\defuzzification.h"

class ParametrReader{
	std::string filename;
    defuzzification &def;
    std::ifstream inp;

    char c;

    int from;
    int to;
    int oldFrom;
    int oldTo;
    int Oto;

    int smallLenght;

    void GetNext();

    void GetSensors();
    void GetAngToGarage();
    void GetAngOfWheel();
    void GetSpeed();

public:
    ParametrReader(std::string filename, defuzzification &def);
	
    void Read();

    int SmallLength();
};
