#include"ParametrReader.h"

ParametrReader::ParametrReader(std::string filename, defuzzification &def): filename(filename), def(def){}

void ParametrReader::Read(){
     inp.open(filename.c_str());

    GetSensors();
    GetAngToGarage();
    GetAngOfWheel();
    GetSpeed();

    inp.close();
}

// Датчики
void ParametrReader::GetSensors(){
    // Близко
    GetNext();

    //Средне
    GetNext();
    smallLenght = from;
    def.add_center_sensor_tr(trapeze(oldFrom, oldFrom, from, oldTo));
    def.add_right_sensor_tr(trapeze(oldFrom, oldFrom, from, oldTo));
    def.add_left_sensor_tr(trapeze(oldFrom, oldFrom, from, oldTo));

    //Далеко
    GetNext();
    def.add_center_sensor_tr(trapeze(oldFrom, Oto, from, oldTo));
    def.add_right_sensor_tr(trapeze(oldFrom, Oto, from, oldTo));
    def.add_left_sensor_tr(trapeze(oldFrom, Oto, from, oldTo));

    def.add_center_sensor_tr(trapeze(from, oldTo, to, to));
    def.add_right_sensor_tr(trapeze(from, oldTo, to, to));
    def.add_left_sensor_tr(trapeze(from, oldTo, to, to));
}

// Угол до гаража
void ParametrReader::GetAngToGarage(){

    //Право
    GetNext();

    //Нет
    GetNext();
    def.add_angle_to_garage_tr(trapeze(oldFrom, oldFrom, from, oldTo));

    //Лево
    GetNext();
    def.add_angle_to_garage_tr(trapeze(oldFrom, Oto, from, oldTo));

    def.add_angle_to_garage_tr(trapeze(from, oldTo, to, to));
}

void ParametrReader::GetAngOfWheel(){
// Угол поворота колёс
    //Право
    GetNext();

    //Нет
    GetNext();
    def.add_angle_of_wheels_tr(trapeze(oldFrom, oldFrom, from, oldTo));

    //Лево
    GetNext();
    def.add_angle_of_wheels_tr(trapeze(oldFrom, Oto, from, oldTo));

    def.add_angle_of_wheels_tr(trapeze(from, oldTo, to, to));
}

//Скорость
void ParametrReader::GetSpeed(){
    //Назад
    GetNext();

    //Вперёд
    GetNext();
    def.add_speed_tr(trapeze(oldFrom, oldFrom, from, oldTo));

    def.add_speed_tr(trapeze(from, oldTo, to, to));
}

void ParametrReader::GetNext(){
    do
        inp >> c;
    while(c != ':');


    Oto = oldTo;
    oldFrom = from;
    oldTo = to;
    inp >> from >> to;
}

void ParametrReader::SmallLength(){
    return smallLenght;
}
