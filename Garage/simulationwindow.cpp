#include "simulationwindow.h"
#include "ui_simulationwindow.h"

SimulationWindow::SimulationWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SimulationWindow)
{
    setAttribute( Qt::WA_DeleteOnClose, true );
    ui->setupUi(this);
    ui->graphicsView->setFocus();

    ui->outputView->setScene(new QGraphicsScene());
    ui->graphicsView->setOutDisplay(new OutputDisplay());
    ui->outputView->scene()->addItem(ui->graphicsView->getOutDisplay());

    ui->inputView->setScene(new QGraphicsScene());
    ui->graphicsView->setInDisplay(new InputDisplay());
    ui->inputView->scene()->addItem(ui->graphicsView->getInDisplay());
}

SimulationWindow::~SimulationWindow()
{
    delete ui;
}

void SimulationWindow::loadScene(const SceneState *sceneState)
{
    ui->graphicsView->loadScene(sceneState);
}

void SimulationWindow::on_actionChange_controller_triggered()
{
    ui->graphicsView->changeController();
}

void SimulationWindow::on_actionStop_Play_triggered()
{
    ui->graphicsView->stopOrPlay();
}

void SimulationWindow::on_actionStep_triggered()
{
    ui->graphicsView->step();
}
