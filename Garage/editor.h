#ifndef EDITOR_H
#define EDITOR_H

#include <QGraphicsView>
#include <QContextMenuEvent>
#include <QAction>
#include <QSlider>
#include "carmodel.h"
#include "garagemodel.h"
#include "obstaclemodel.h"
#include "scenestate.h"
#include "selection.h"
#include <string>

class Editor: public QGraphicsView
{
    Q_OBJECT
    QGraphicsScene* scene;
    SceneState* sceneState;
    Selection* selection;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
    void contextMenuEvent(QContextMenuEvent* event) Q_DECL_OVERRIDE;
    QGraphicsItem * selectedItem;
    QPointF mousePos;

    QSlider * zoomSlider;
    bool dragging;
    QPointF dragPoint;

public slots:
    void deleteObstacle();
    void addObstacle();
    void changeScale(int val);
public:
    Editor(QWidget *parent = 0);
    ~Editor();
    void selectItem(QGraphicsItem *item);
    void deselectItem();
    void resetScene();

    void setSlider(QSlider* slider);
    SceneState* getSceneState();
    std::string saveScene();
    void loadScene(std::string s);
};

#endif // EDITOR_H
