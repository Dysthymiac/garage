#ifndef SCENESTATE_H
#define SCENESTATE_H

#include "carmodel.h"
#include "garagemodel.h"
#include "obstaclemodel.h"
#include "carconstants.h"
#include <QPointF>
#include <QPolygonF>





class SceneState
{
    CarModel * car;
    GarageModel * garage;
    QList<ObstacleModel*> obstacles;
    QList<ObstacleModel*> borders;

public:
    SceneState(CarModel* c=0, GarageModel* g=0, QList<ObstacleModel*> o=QList<ObstacleModel*>()):car(c),garage(g),obstacles(o)
    {
        if(car==0)
            car = new CarModel();
        if(garage==0)
            garage = new GarageModel();
        ObstacleModel * om = new ObstacleModel(QRectF(0,0,10,CarConstants::sceneHeight));
        om->setPos(CarConstants::sceneWidth/2, -CarConstants::sceneHeight/2);
        borders << om;
        om = new ObstacleModel(QRectF(0,0,10,CarConstants::sceneHeight));
        om->setPos(-10-CarConstants::sceneWidth/2, -CarConstants::sceneHeight/2);
        borders << om;
        om = new ObstacleModel(QRectF(0,0,CarConstants::sceneWidth,10));
        om->setPos(-CarConstants::sceneWidth/2, CarConstants::sceneHeight/2);
        borders << om;

    }
    ~SceneState()
    {
        /*delete car;
        delete garage;
        qDeleteAll(obstacles);*/
    }
    bool isBorder(QGraphicsItem* i)
    {
        return i==borders[0]||i==borders[1]||i==borders[2];
    }
    QList<ObstacleModel*> getBorders()
    {
        return borders;
    }

    SceneState* clone() const
    {
        SceneState* c = new SceneState();
        c->setCar(new CarModel(*car));
        c->setGarage(new GarageModel(*garage));
        QList<ObstacleModel*> o;
        for(int i = 0; i < obstacles.size(); ++i)
            o << new ObstacleModel(*obstacles[i]);
        c->setObstacles(o);
        return c;
    }

    void setCar(CarModel * c)
    {
        //if(car)
        //    delete car;
        car = c;
    }

    void setGarage(GarageModel * g)
    {
        //if(garage)
        //   delete garage;
        garage = g;
    }

    void setObstacles(QList<ObstacleModel*>& o)
    {
        qDeleteAll(obstacles);
        obstacles = o;
    }

    CarModel * getCar()
    {
        return car;
    }

    GarageModel * getGarage()
    {
        return garage;
    }

    QList<ObstacleModel*>& getObstacles()
    {
        return obstacles;
    }
};

#endif // SCENESTATE_H
