#include "editor.h"

#include <QMouseEvent>
#include <QDebug>
#include <QMenu>
#include <QScrollBar>
#include <sstream>
#include "carconstants.h"

Editor::Editor(QWidget *parent)
    : QGraphicsView(parent), selectedItem(0)
{
    scene = new QGraphicsScene(this);
    scene->setSceneRect(-CarConstants::sceneWidth/2,-CarConstants::sceneHeight/2, CarConstants::sceneWidth, CarConstants::sceneHeight);


    selection = new Selection();

    this->setScene(scene);

}

Editor::~Editor()
{
    /*scene->clear();
    delete selection;
    delete car;
    delete garage;
    delete scene;*/
}

void Editor::resetScene()
{
    scene->clear();
    sceneState = new SceneState();
    selection = new Selection();
    scene->addItem(sceneState->getCar());
    scene->addItem(sceneState->getGarage());
    scene->addItem(sceneState->getBorders().at(0));
    scene->addItem(sceneState->getBorders().at(1));
    scene->addItem(sceneState->getBorders().at(2));
}

std::string Editor::saveScene()
{
    std::ostringstream ss;
    ss << sceneState->getCar()->pos().x() << " " << sceneState->getCar()->pos().y() << " "<< sceneState->getCar()->rotation() << std::endl;
    //ss << sceneState->getGarage()->pos().x() << " "<< sceneState->getGarage()->pos().y() << " "<< sceneState->getGarage()->rotation() << std::endl;
    ss << sceneState->getObstacles().size() << std::endl;
    foreach(ObstacleModel* i, sceneState->getObstacles())
    {
        ss << i->pos().x() << " "<< i->pos().y()<< " " << i->rotation()<< " " << i->bottomRight().x() << " " << i->bottomRight().y();

        ss << std::endl;
    }
    return ss.str();
}

void Editor::loadScene(std::string s)
{
    resetScene();
    std::istringstream ss(s);
    qreal x, y, r;
    ss >> x >> y >> r;
    sceneState->getCar()->setPos(x,y);
    sceneState->getCar()->setRotation(r);
    //ss >> x >> y >> r;
    //sceneState->getGarage()->setPos(x,y);
    //sceneState->getGarage()->setRotation(r);

    int res;
    ss >> res;

    for(int i = 0; i < res; ++i)
    {
        ss >> x >> y >> r;
        QRectF p;
        qreal px, py;

        ss >> px >> py;
        p.setBottomRight(QPointF(px, py));

        ObstacleModel* om = new ObstacleModel(p);
        om->setPos(x, y);
        om->setRotation(r);
        sceneState->getObstacles().append(om);
        scene->addItem(om);
    }

}


void Editor::setSlider(QSlider *slider)
{
    zoomSlider = slider;
    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(changeScale(int)));
}

SceneState *Editor::getSceneState()
{
    return sceneState;
}

void Editor::selectItem(QGraphicsItem* item)
{

    if(item==selection || sceneState->isBorder(item) || (selection && item==selection->selectedItem()) || item==sceneState->getGarage())
        return;
    if(selection->scene()!=scene)
        scene->addItem(selection);
    selection->setSelectedItem(item);

}
void Editor::deselectItem()
{
    if(selection->scene() == scene)
    {
        selection->clearSelection();
        scene->removeItem(selection);
    }
}

void Editor::mouseReleaseEvent(QMouseEvent *event)
{

    QGraphicsView::mouseReleaseEvent(event);
    dragging = false;
}

void Editor::mousePressEvent(QMouseEvent *event)
{
    QGraphicsView::mousePressEvent(event);
    if(event->isAccepted())
        return;


    QGraphicsItem* i = scene->itemAt(this->mapToScene(event->pos()),QTransform());//this->itemAt(event->pos());
    if(i)
        selectItem(i);
    else
    {
        deselectItem();
        dragPoint=event->pos();
        dragging = true;
    }


}

void Editor::mouseMoveEvent(QMouseEvent *event)
{
    QGraphicsView::mouseMoveEvent(event);
    if(dragging)
    {

        QPointF d(event->x()-dragPoint.x(), event->y()-dragPoint.y());
        this->verticalScrollBar()->setValue(verticalScrollBar()->value()-d.y());
        this->horizontalScrollBar()->setValue(horizontalScrollBar()->value()-d.x());

        dragPoint = event->pos();

    }
}

void Editor::wheelEvent(QWheelEvent *event)
{

    qreal d = 1.0+event->angleDelta().y()/1200.0;
    this->scale(d,d);
    if(zoomSlider)
        zoomSlider->setValue(this->transform().m11()*100);

}

void Editor::contextMenuEvent(QContextMenuEvent *event)
{
    QGraphicsItem* i = this->itemAt(event->x(), event->y());
    CarModel * car = sceneState->getCar();
    GarageModel * garage = sceneState->getGarage();

    if(i==car||i==garage||(i==selection && (selection->selectedItem()==car||selection->selectedItem()==garage)))
        return;

    QMenu* menu = new QMenu(this);

    if(i)
        menu->addAction("&Delete obstacle", this, SLOT(deleteObstacle()));
    else
        menu->addAction("&Add obstacle", this, SLOT(addObstacle()));
    mousePos=event->pos();
    menu->popup(mapToGlobal(event->pos()));


}

void Editor::deleteObstacle()
{
    if(!selection)
        return;
    ObstacleModel* m = dynamic_cast<ObstacleModel*>(selection->selectedItem());
    sceneState->getObstacles().removeAll(m);
    scene->removeItem(m);
    deselectItem();
}

void Editor::addObstacle()
{
    ObstacleModel* m = new ObstacleModel();
    sceneState->getObstacles() << m;

    m->setPos(this->mapToScene(mousePos.toPoint()));
    scene->addItem(m);
}

void Editor::changeScale(int val)
{
    this->setTransform(this->transform().fromScale(val/100.0, val/100.0));
}
