#include "selection.h"

#include <QPainter>
#include <QtCore/QtMath>
#include <QDebug>
#include <QGraphicsScene>

Selection::Selection(QGraphicsItem * i):item(i),dragging(false), rotating(false)
{
    node = 0;
}

Selection::~Selection()
{
}

QRectF Selection::boundingRect() const
{
    /*if(!item)
        return QRectF(0,0,0,0);
    else
        return QRectF(item->boundingRect().left(),
                      item->boundingRect().top(),
                      item->boundingRect().width()+16,
                      item->boundingRect().height());*/
    return shape().boundingRect();

}

QPainterPath Selection::shape() const
{
    if(!item)
        return QPainterPath();
    else
    {
        QPainterPath p;
        p.addPath(item->shape());
        p.addEllipse(QPointF(item->boundingRect().right()+10, 0),5,5);
        return p;
    }
}

void Selection::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if(!item)
        return;
    painter->setBrush(QColor(255,255,255,100));
    painter->drawPath(item->shape());

    painter->setBrush(QBrush(Qt::red));
    QPainterPath p;
    p.addEllipse(QPointF(item->boundingRect().right()+10, 0),5,5);
    painter->drawPath(p);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}



void Selection::setSelectedItem(QGraphicsItem *item)
{
    if(item==this->item)
        return;

    clearSelection();
    this->item = item;
    this->setRotation(item->rotation());
    this->setPos(item->pos());

    ObstacleModel* om = dynamic_cast<ObstacleModel*>(item);

    if(om)
        node = new ResizeNode(this, om);

}

void Selection::clearSelection()
{
    item = 0;

    //this->childItems().removeAll(node);
    //this->childItems().removeAll(node);
    if(node!=0)
    {
        node->setParentItem(NULL);
        node->scene()->removeItem(node);
        node = 0;
    }
    //delete node;

}

void Selection::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    dragging = false;
    rotating = false;
    Q_UNUSED(event);
}

void Selection::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QPainterPath rotateCircle;
    rotateCircle.addEllipse(QPointF(item->boundingRect().right()+10, 0),5,5);

    if(rotateCircle.contains(event->pos()))
        rotating = true;
    else
    {
        dragPoint=this->scenePos()-mapToScene(QPointF(event->pos()));
        dragging = true;
    }

}

void Selection::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if(rotating)
    {
        QPointF p(event->scenePos().x() - this->scenePos().x(),event->scenePos().y() - this->scenePos().y());
        setRotation(qRadiansToDegrees(qAtan2(p.y(), p.x())));
        item->setRotation(rotation());
        scene()->update();
    }
    else if(dragging)
    {
        setPos(mapToItem(this, event->scenePos()+dragPoint));
        item->setPos(pos());
        scene()->update();
    }
}

QGraphicsItem *Selection::selectedItem() const
{
    return item;
}


