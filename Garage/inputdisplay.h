#ifndef INPUTDISPLAY_H
#define INPUTDISPLAY_H

#include "sensordata.h"

#include <QGraphicsObject>



class InputDisplay : public QGraphicsObject
{
    SensorData sd;
    qreal width;
    qreal height;
public:
    InputDisplay(QGraphicsItem *i=0);

    void updateParams(SensorData sd);
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    qreal getWidth() const;
    void setWidth(const qreal &value);
    qreal getHeight() const;
    void setHeight(const qreal &value);
private:
    void drawBar(QPainter *painter, qreal val,qreal r, QPointF pos, qreal w, qreal h, bool topToDown = false);
    void drawAngle(QPainter *painter, QRectF rect, qreal angle);
};

#endif // INPUTDISPLAY_H
