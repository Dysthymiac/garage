#include "obstaclemodel.h"

#include <QDebug>
#include <QPainter>


ObstacleModel::ObstacleModel(const ObstacleModel &o):QGraphicsItem(),rect(o.boundingRect())
{
    setPos(o.pos());
    setRotation(o.rotation());
    color.setRgb(0,0,0);
    //color.setAlpha(100);
}
ObstacleModel::ObstacleModel(QRectF r):rect(r)
{
    color.setRgb(0,0,0);
}

ObstacleModel::ObstacleModel()
{
    rect.setBottomRight(QPointF(20, 20));
    color.setRgb(0,0,0);
    //color.setAlpha(100);
}

QRectF ObstacleModel::boundingRect() const
{
    return rect;
}

QPainterPath ObstacleModel::shape() const
{
    QPainterPath p;
    p.addRect(rect);
    p.closeSubpath();
    return p;
}

void ObstacleModel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(color);
    painter->drawRect(rect);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void ObstacleModel::setBottomRight(QPointF point)
{
    rect.setBottomRight(point);
}

QPointF ObstacleModel::bottomRight()
{
    return rect.bottomRight();
}

