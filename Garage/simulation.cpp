#include "keyboardcarcontroller.h"
#include "systemcarcontr.h"
#include "simulation.h"
#include "carconstants.h"


#include <QMouseEvent>
#include <QDebug>
#include <QMenu>
#include <QScrollBar>
#include <QMessageBox>


Simulation::Simulation(QWidget *parent)
    : QGraphicsView(parent), outDisplay(0)
{
    scene = new QGraphicsScene(this);
    scene->setSceneRect(-CarConstants::sceneWidth/2,-CarConstants::sceneHeight/2, CarConstants::sceneWidth, CarConstants::sceneHeight);

    this->setScene(scene);
    this->animationTimer = new QTimer(this);
    connect(animationTimer, SIGNAL(timeout()), this, SLOT(simulationUpdate()));

}

void Simulation::loadScene(const SceneState *sceneState)
{
    this->sceneState = sceneState->clone();
    this->sceneState->getCar()->setShowSensors(true);
    scene->addItem(this->sceneState->getCar());
    scene->addItem(this->sceneState->getGarage());

    scene->addItem(this->sceneState->getBorders().at(0));
    scene->addItem(this->sceneState->getBorders().at(1));
    scene->addItem(this->sceneState->getBorders().at(2));
    for(QList<ObstacleModel*>::iterator i = this->sceneState->getObstacles().begin(); i!=this->sceneState->getObstacles().end();++i)
        scene->addItem(*i);
    animationTimer->start((int)(1000.0/60));

    KeyboardCarController* c = new KeyboardCarController(this->sceneState->getCar());

    connect(this,  SIGNAL(keyPressSignal(QKeyEvent*)), c, SLOT(keyPressEvent(QKeyEvent*)));
    connect(this,  SIGNAL(keyReleaseSignal(QKeyEvent*)), c, SLOT(keyReleaseEvent(QKeyEvent*)));

    controller = c;
    manual = true;
}

Simulation::~Simulation()
{
    scene->clear();
    if(animationTimer)
        animationTimer->stop();
    delete controller;
    delete animationTimer;
    delete sceneState;
    delete scene;
}

void Simulation::changeController()
{
    if(manual)
    {
        disconnect(this,  SIGNAL(keyPressSignal(QKeyEvent*)), controller, SLOT(keyPressEvent(QKeyEvent*)));
        disconnect(this,  SIGNAL(keyReleaseSignal(QKeyEvent*)), controller, SLOT(keyReleaseEvent(QKeyEvent*)));

        //delete controller;
        controller = new SystemCarController(this->sceneState->getCar());

        manual = false;

    }
    else
    {
        KeyboardCarController* c = new KeyboardCarController(this->sceneState->getCar());

        connect(this,  SIGNAL(keyPressSignal(QKeyEvent*)), c, SLOT(keyPressEvent(QKeyEvent*)));
        connect(this,  SIGNAL(keyReleaseSignal(QKeyEvent*)), c, SLOT(keyReleaseEvent(QKeyEvent*)));

        controller = c;
        manual = true;
    }
}

void Simulation::stopOrPlay()
{
    if(animationTimer->isActive())
        animationTimer->stop();
    else
        animationTimer->start();
}

void Simulation::step()
{
    if(animationTimer->isActive())
        animationTimer->stop();
    simulationUpdate();
}

SensorData Simulation::getSensorData()
{
    SensorData sd;
    sd.direction=(controller->getVelocity()>0)?1:-1;

    QPointF p1 = sceneState->getCar()->mapToScene(sceneState->getCar()->getFrontCenter());
    QPointF p2 = sceneState->getCar()->mapToScene(sceneState->getCar()->getBackCenter());
    QPointF p3 = sceneState->getGarage()->mapToScene(sceneState->getGarage()->getBackPoint());
    //QPointF p4 = sceneState->getGarage()->mapToScene(sceneState->getGarage()->getBackPoint());

    sd.angle = QLineF(p1, p2).angleTo(QLineF(p2, p3));
    if(sd.angle > 180)
        sd.angle -= 360;
    sceneState->getCar()->updateSensors();

    sd.front1 = sceneState->getCar()->getFrontSensors()[0];
    sd.front2 = sceneState->getCar()->getFrontSensors()[1];
    sd.front3 = sceneState->getCar()->getFrontSensors()[2];

    sd.back1 = sceneState->getCar()->getBackSensors()[0];
    sd.back2 = sceneState->getCar()->getBackSensors()[1];
    sd.back3 = sceneState->getCar()->getBackSensors()[2];


    return sd;
}

void Simulation::keyPressEvent(QKeyEvent *event)
{

    emit(keyPressSignal(event));
    //QGraphicsView::keyPressEvent(event);
}

void Simulation::keyReleaseEvent(QKeyEvent *event)
{
    emit(keyReleaseSignal(event));
    //QGraphicsView::keyReleaseEvent(event);
}
OutputDisplay *Simulation::getOutDisplay() const
{
    return outDisplay;
}

void Simulation::setOutDisplay(OutputDisplay *value)
{
    outDisplay = value;
    outDisplay->updateParams(0,0);
}

InputDisplay *Simulation::getInDisplay() const
{
    return inDisplay;
}

void Simulation::setInDisplay(InputDisplay *value)
{
    inDisplay = value;
    inDisplay->updateParams(SensorData());
}

void Simulation::simulationUpdate()
{

    SensorData sd = getSensorData();

    controller->update(sd);
    this->centerOn(sceneState->getCar());

    if(outDisplay)
        outDisplay->updateParams(controller->getVelocity(), controller->getTheta());
    if(inDisplay)
        inDisplay->updateParams(sd);

    QPointF cp = sceneState->getCar()->pos();
    QPointF gp = sceneState->getGarage()->pos();

    if(QLineF(cp, gp).length()<20)
    {

        QMessageBox msgBox;
        msgBox.setText("The car is in the garage.");
        msgBox.exec();
        animationTimer->stop();
    }
}
