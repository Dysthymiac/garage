#pragma once

#include "carcontroller.h"
#include "defuzzification_folder/defuzzification.h"
#include "./Parser/header.h"
#include "./ParametrReader/ParametrReader.h"

class SystemCarController : public CarController
{
    defuzzification *def;

    int repeat;
    int minLength;
public:
    SystemCarController(CarModel* c);
    ~SystemCarController();

    void updateController(SensorData sd);

    virtual void reset();
};
