#ifndef CARCONTROLLER_H
#define CARCONTROLLER_H

#include "carmodel.h"
#include "sensordata.h"
#include <QObject>
#include <QDebug>

class CarController:public QObject
{
    Q_OBJECT
protected:
    CarModel* car;
    qreal velocity;
    qreal theta;
public:

    CarController(CarModel* c):car(c)
    {
        reset();
    }

    virtual void update(SensorData sd)
    {
        updateController(sd);
        updateCar();
    }

    virtual void updateController(SensorData sd) = 0;
    virtual void updateCar();

    virtual void reset()
    {
        velocity = 0;
        theta = 45;
    }
    qreal getVelocity() const;
    qreal getTheta() const;
};

#endif // CARCONTROLLER_H
