#include "keyboardcarcontroller.h"

#include "carconstants.h"


void KeyboardCarController::updateController(SensorData sd)
{
    Q_UNUSED(sd);

    if(forward)
        velocity=(velocity<CarConstants::maxVelocity)?velocity+0.1:CarConstants::maxVelocity;
    else if(backward)
        velocity=(velocity>-CarConstants::maxVelocity)?velocity-0.1:-CarConstants::maxVelocity;
    else
        velocity=(qAbs(velocity)>0.1)?velocity*0.8:0;

    if(right)
        theta=(theta<90)?theta+0.5:90;
    else if(left)
        theta=(theta>0)?theta-0.5:0;
}

void KeyboardCarController::keyPressEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Up||event->key()==Qt::Key_W)
        forward=true;
    else if(event->key()==Qt::Key_Down||event->key()==Qt::Key_S)
        backward=true;
    else if(event->key()==Qt::Key_Left||event->key()==Qt::Key_A)
        left=true;
    else if(event->key()==Qt::Key_Right||event->key()==Qt::Key_D)
        right=true;
}

void KeyboardCarController::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Up||event->key()==Qt::Key_W)
        forward=false;
    else if(event->key()==Qt::Key_Down||event->key()==Qt::Key_S)
        backward=false;
    else if(event->key()==Qt::Key_Left||event->key()==Qt::Key_A)
        left=false;
    else if(event->key()==Qt::Key_Right||event->key()==Qt::Key_D)
        right=false;
}
