#-------------------------------------------------
#
# Project created by QtCreator 2015-05-24T15:02:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Garage
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    carconstants.cpp \
    carcontroller.cpp \
    carmodel.cpp \
    editor.cpp \
    garagemodel.cpp \
    inputdisplay.cpp \
    keyboardcarcontroller.cpp \
    mypair.cpp \
    mypoint.cpp \
    obstaclemodel.cpp \
    outputdisplay.cpp \
    resizenode.cpp \
    scenestate.cpp \
    selection.cpp \
    sensordata.cpp \
    simulation.cpp \
    simulationwindow.cpp \
    systemcarcontr.cpp \
    Parser/parser.cpp \
    Parser/rule.cpp \
    ParametrReader/ParametrReader.cpp \
    defuzzification_folder/defuzzification.cpp \
    defuzzification_folder/graph.cpp \
    defuzzification_folder/trapeze.cpp

HEADERS  += mainwindow.h \
    carconstants.h \
    carcontroller.h \
    carmodel.h \
    editor.h \
    garagemodel.h \
    inputdisplay.h \
    keyboardcarcontroller.h \
    mypair.h \
    mypoint.h \
    obstaclemodel.h \
    outputdisplay.h \
    resizenode.h \
    scenestate.h \
    selection.h \
    sensordata.h \
    simulation.h \
    simulationwindow.h \
    systemcarcontr.h \
    Parser/header.h \
    Parser/included.h \
    ParametrReader/ParametrReader.h \
    defuzzification_folder/defuzzification.h \
    defuzzification_folder/graph.h \
    defuzzification_folder/trapeze.h

FORMS += \
    mainwindow.ui \
    simulationwindow.ui

DISTFILES += \
    Garage.pro.user \
    ParametrReader/ranges.txt
