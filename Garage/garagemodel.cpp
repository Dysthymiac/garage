#include "garagemodel.h"

#include <QDebug>
#include <QPainter>
#include "carconstants.h"

GarageModel::GarageModel()
{
    initPolygon();

}

GarageModel::GarageModel(const GarageModel &g):QGraphicsItem()
{
    setPos(g.pos());
    setRotation(g.rotation());
    initPolygon();
}

void GarageModel::initPolygon()
{
    polygon_shape << QPointF(-CarConstants::sceneWidth/2, -40) <<
                     QPointF(CarConstants::sceneWidth/2, -40) <<
                     QPointF(CarConstants::sceneWidth/2, 40) <<
                     QPointF(30, 40) <<
                     QPointF(30, -35) <<
                     QPointF(-30, -35) <<
                     QPointF(-30, 40) <<
                     QPointF(-CarConstants::sceneWidth/2, 40);
    color.setRgb(0, 0, 0);
    //color.setAlpha(100);

    setPos(0, 40-CarConstants::sceneHeight/2);
}

QRectF GarageModel::boundingRect() const
{
    return shape().boundingRect();
}

QPainterPath GarageModel::shape() const
{
    QPainterPath p;
    p.addPolygon(polygon_shape);

    return p;
}

void GarageModel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(color);
    painter->drawConvexPolygon(polygon_shape);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}
QPointF GarageModel::getBackPoint()
{
    return QPointF(0, -35);
}
QPointF GarageModel::getFrontPoint()
{
    return QPointF(0, 35);
}
