#ifndef SELECTION_H
#define SELECTION_H

#include "resizenode.h"

#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include "obstaclemodel.h"


class Selection : public QGraphicsItem
{
public:
    Selection(QGraphicsItem *i=0);
    ~Selection();
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void setSelectedItem(QGraphicsItem * item);
    void clearSelection();

    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

    QGraphicsItem* selectedItem() const;

    enum { Type = UserType + 3 };

    int type() const
    {
       return Type;
    }
protected:
    ResizeNode* node;
    bool obstacleSelected;
    QGraphicsItem* item;
    bool dragging;
    bool rotating;
    QPointF dragPoint;
};

#endif // SELECTION_H
