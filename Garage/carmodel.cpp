#include "carmodel.h"
#include "obstaclemodel.h"

#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPainter>
#include "carconstants.h"
#include <QtMath>

CarModel::CarModel():QGraphicsItem()
{
    initPolygon();

}

CarModel::CarModel(const CarModel &c):QGraphicsItem()
{
    setPos(c.pos());
    setRotation(c.rotation());
    initPolygon();
}

void CarModel::initPolygon()
{
    polygon_shape << QPointF(0, -CarConstants::carHeight/2) <<
                     QPointF(CarConstants::carWidth/2, -CarConstants::carHeight*0.3) <<
                     QPointF(CarConstants::carWidth/2, CarConstants::carHeight/2) <<
                     QPointF(-CarConstants::carWidth/2, CarConstants::carHeight/2) <<
                     QPointF(-CarConstants::carWidth/2, -CarConstants::carHeight*0.3);
    color.setRgb(255, 0, 0);
    frontSensors << 0 << 0 << 0;
    backSensors << 0 << 0 << 0;

    backLines << QLineF() << QLineF() << QLineF();
    frontLines << QLineF() << QLineF() << QLineF();
    //Front center
    QLineF l(0, -CarConstants::carHeight/2, 0, -CarConstants::sensorsRange-CarConstants::carHeight/2);
    frontLines[1] = l;
    //Front left
    l = QLineF(0, 0, 0, -CarConstants::sensorsRange-CarConstants::carHeight/2);
    l.setAngle(l.angle()+CarConstants::sensorsAngle);
    l.translate(-CarConstants::carWidth/2, -CarConstants::carHeight*0.3);
    frontLines[0] = l;
    //Front right
    l = QLineF(0, 0, 0, -CarConstants::sensorsRange-CarConstants::carHeight/2);
    l.setAngle(l.angle()-CarConstants::sensorsAngle);
    l.translate(CarConstants::carWidth/2, -CarConstants::carHeight*0.3);
    frontLines[2] = l;

    //Back center
    l = QLineF(0, CarConstants::carHeight/2, 0, CarConstants::sensorsRange+CarConstants::carHeight/2);
    backLines[1] = l;
    //Back left
    l.setAngle(l.angle()+CarConstants::sensorsAngle);
    l.translate(CarConstants::carWidth/2, 0);
    backLines[2] = l;
    //Back right
    l = QLineF(0, CarConstants::carHeight/2, 0, CarConstants::sensorsRange+CarConstants::carHeight/2);
    l.setAngle(l.angle()-CarConstants::sensorsAngle);
    l.translate(-CarConstants::carWidth/2, 0);
    backLines[0] = l;
}
QList<qreal> CarModel::getBackSensors() const
{
    return backSensors;
}

QList<qreal> CarModel::getFrontSensors() const
{
    return frontSensors;
}


QRectF CarModel::boundingRect() const
{
    qreal w = 2*CarConstants::sensorsRange*qSin(qDegreesToRadians((double)CarConstants::sensorsAngle));
    qreal h = CarConstants::sensorsRange;
    QRectF r1(-CarConstants::carWidth/2-w/2, -CarConstants::carHeight-h, w+CarConstants::carWidth, h);
    QRectF r2(-CarConstants::carWidth/2-w/2, CarConstants::carHeight/2, w+CarConstants::carWidth, h);
    QPainterPath p = shape();
    p.addRect(r1);
    p.addRect(r2);

    return p.boundingRect();
}

QPainterPath CarModel::shape() const
{
    QPainterPath p;
    p.addPolygon(polygon_shape);
    return p;
}

qreal CarModel::updateSensor(QLineF l)
{
    QLineF l1(mapToScene(l.p1()), mapToScene(l.p2()));

    QPainterPath p;
    p.addPolygon(QPolygonF()<<l1.p1()<<l1.p2()<<l1.p2()+QPointF(1,1)<<l1.p1()+QPointF(1,1));

    qreal dist = l1.length();

    QList<QGraphicsItem *> itemsInPath = scene()->items(p, Qt::IntersectsItemShape);

    QPainterPath *closest = 0;
    QPointF intersection;

    foreach (QGraphicsItem *item, itemsInPath)
    {
        if(item==this)
            continue;
        QPainterPath* pp = new QPainterPath(p.intersected(item->mapToScene(item->shape())));

        QPointF p2 = pp->pointAtPercent(0);
        //qreal d = (p2.x()-l1.p1().x())/(l1.p2().x()-l1.p1().x())-(p2.y()-l1.p1().y())/(l1.p2().y()-l1.p1().y());
        //if(qAbs(d)>0.1)
            //continue;
        if(pp->elementCount()>0 && (closest==0 || QLineF(l1.p1(), p2).length()<dist))
        {

            closest = pp;
            intersection = p2;
            dist = QLineF(l1.p1(), intersection).length();
        }
    }
    if(dist > CarConstants::sensorsRange)
        dist = CarConstants::sensorsRange;
    return dist;
}

void CarModel::updateSensors()
{

    //Front center
    frontSensors[1] = updateSensor(frontLines[1]);
    //Front left
    frontSensors[0] = updateSensor(frontLines[0]);
    //Front right
    frontSensors[2] = updateSensor(frontLines[2]);

    //Back center
    backSensors[1] = updateSensor(backLines[1]);
    //Back left
    backSensors[2] = updateSensor(backLines[2]);
    //Back right
    backSensors[0] = updateSensor(backLines[0]);

}

void CarModel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(color);
    painter->drawConvexPolygon(polygon_shape);

    if(showSensors)
    {
        painter->setPen(Qt::blue);

        QLineF l = frontLines[1];
        l.setLength(frontSensors[1]);
        painter->drawLine(l);

        l = frontLines[2];;
        l.setLength(frontSensors[2]);
        painter->drawLine(l);

        l = frontLines[0];;
        l.setLength(frontSensors[0]);
        painter->drawLine(l);

        l = backLines[1];;
        l.setLength(backSensors[1]);
        painter->drawLine(l);

        l = backLines[0];;
        l.setLength(backSensors[0]);
        painter->drawLine(l);

        l = backLines[2];;
        l.setLength(backSensors[2]);
        painter->drawLine(l);
    }


    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QPointF CarModel::getRightFrontWheel()
{
    return QPointF(CarConstants::carWidth/2, -CarConstants::carHeight*0.3);
}

QPointF CarModel::getLeftFrontWheel()
{
    return QPointF(-CarConstants::carWidth/2, -CarConstants::carHeight*0.3);
}

QPointF CarModel::getRightBackWheel()
{
    return QPointF(CarConstants::carWidth/2, CarConstants::carHeight*0.3);
}

QPointF CarModel::getLeftBackWheel()
{
    return QPointF(-CarConstants::carWidth/2, CarConstants::carHeight*0.3);
}

QPointF CarModel::getBackCenter()
{
    return QPointF(0, CarConstants::carHeight*0.3);
}

QPointF CarModel::getFrontCenter()
{
    return QPointF(0, -CarConstants::carHeight*0.3);
}
bool CarModel::getShowSensors() const
{
    return showSensors;
}

void CarModel::setShowSensors(bool value)
{
    showSensors = value;
}

