#ifndef SIMULATIONWINDOW_H
#define SIMULATIONWINDOW_H

#include "scenestate.h"

#include <QMainWindow>

namespace Ui {
class SimulationWindow;
}

class SimulationWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SimulationWindow(QWidget *parent = 0);
    ~SimulationWindow();
    void loadScene(const SceneState* sceneState);
private slots:
    void on_actionChange_controller_triggered();

    void on_actionStop_Play_triggered();

    void on_actionStep_triggered();

private:
    Ui::SimulationWindow *ui;
};

#endif // SIMULATIONWINDOW_H
